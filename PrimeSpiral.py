from tkinter import *
import tkinter as tk
import time

multiplier = 29
currentPosX = 375
currentPosY = 375

offsetList = [(0,0),(1,0),(0,-1),(-1,0),(0,1)]
offsetListIndex = 0
offsetUsedCounter = 0
currentRepeatNum = 1


def FindX(currentPosX):

    currentPosX = currentPosX + offsetList[offsetListIndex][0] * multiplier

    return currentPosX


def FindY(currentPosY, currentRepeatNum, offsetUsedCounter, offsetListIndex):

    if (offsetUsedCounter == currentRepeatNum and (offsetListIndex == 0 or offsetListIndex == 1 or offsetListIndex == 3)):
        offsetUsedCounter = 1
        offsetListIndex = IncerementOffsetIndex(offsetListIndex)
    elif (offsetUsedCounter == currentRepeatNum and (offsetListIndex == 2 or offsetListIndex == 4)):
        currentRepeatNum += 1
        offsetUsedCounter = 1
        offsetListIndex = IncerementOffsetIndex(offsetListIndex)
    else:
        offsetUsedCounter = offsetUsedCounter + 1

    currentPosY = currentPosY + offsetList[offsetListIndex][1] * multiplier

    return currentPosY, currentRepeatNum, offsetUsedCounter, offsetListIndex

def IncerementOffsetIndex(index):
    if (index < 4):
        return index + 1
    else:
        return 1

def FindIfPrime(num):
    if num > 1:
       for i in range(2,num):
           if (num % i) == 0:
               break
       else:
           return True
    else:
       return False

class PrimeNumber:
    def __init__(self,number, posX, posY):
        self.number = number  
        self.posX = posX
        self.posY = posY
    
window = tk.Tk()
window.minsize(750,750)
window.maxsize(750,750)

PrimeList = []

# Iterate through our range of numbers
for i in range(601):
    if (i != 0):
        currentPosY, currentRepeatNum, offsetUsedCounter, offsetListIndex = FindY(currentPosY, currentRepeatNum, offsetUsedCounter, offsetListIndex)
        currentPosX = FindX(currentPosX)

        # Find if prime
        if (FindIfPrime(i)):
            PrimeList.append(PrimeNumber(i, currentPosX, currentPosY)) # add to list
        else:
            number = tk.Label(window, text=str(i)).place(x=currentPosX, y=currentPosY, anchor="center") # just draw

        
# Draw prime numbers in green
for i in range(len(PrimeList)):
    number = tk.Label(window, text=str(PrimeList[i].number), fg="green").place(x=PrimeList[i].posX, y=PrimeList[i].posY, anchor="center")

window.mainloop()
