## Name
Python Prime Spiral

## Description
It is a fun little project that uses tkinter library to produce a graphical representation of number spiral with prime numbers.
Written in Python 3.8.0

## How to use it
There is virtual environment folder called "prime-spiral" that is in the git repository. This environment was created using "venv" (virtualenv) and it includes the required libraries for the program to run.

To activate the environment you need:
- to have a virtual environment library (if you don't already)
- execute this command:
	$source prime-spiral/bin/activate

## Roadmap
- Animations might be added in the future
- Option for users to change the parameters (window size, number range, etc.)

## Authors and acknowledgment
Ozgur Aziz

## License
Open source project. Anyone can use it for personal or commercial use.
